import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

import { AdmitService } from '../../models/nurse/admit';
import createSchema from '../../schema/nurse/create';
import updateSchema from '../../schema/nurse/update';
import listSchema from '../../schema/nurse/list';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const admitService = new AdmitService();

  fastify.post('/admit', {
    preHandler: [
      fastify.guard.role('nurse'),
      fastify.guard.scope('nurse.create')],
    schema: createSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      // jwt decoded
      const userId: any = request.user.sub;

      const body: any = request.body;
      const {
        an,
        hn,
        vn,
        insurance_id,
        department_id,
        pre_diag,
        ward_id,
        bed_id,
        admit_date,
        admit_time,
        doctor_id,
        admit_by
      } = body;

      const data: any = {
        an,
        hn,
        vn,
        insurance_id,
        department_id,
        pre_diag,
        ward_id,
        bed_id,
        admit_date,
        admit_time,
        doctor_id,
        admit_by,
        create_by: userId
      }

      await admitService.save(db, data);

      return reply.status(StatusCodes.CREATED).send({ ok: true });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  fastify.put('/admit/:id', {
    preHandler: [
      fastify.guard.role('admin'),
      fastify.guard.scope('order.create')],
    // schema: updateSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const req:any = request
      const data:any = req.body
      const id:any = req.params.id

      await admitService.update(db, id, data)
      return reply.status(StatusCodes.OK).send({ ok: true });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  fastify.delete('/admit/:id', {
    preHandler: [
      fastify.guard.role('admin'),
      fastify.guard.scope('order.create')],
    // schema: updateSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const req:any = request
      const id:any = req.params.id

      await admitService.delete(db, id)
      return reply.status(StatusCodes.OK).send({ ok: true });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  fastify.get('/admit', {
    preHandler: [
      fastify.guard.role('nurse'),
      fastify.guard.scope('nurse.ward')],
    schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const req: any = request.query
      const ward_id: any = req.ward_id
      const query = req.query
      const limit: any = req.limit
      const offset: any = req.offset

      const results: any = await admitService.listActive(db, ward_id, query, limit, offset);

      // total
      const resultTotal: any = await admitService.listActiveTotal(db, ward_id, query)

      return reply.status(StatusCodes.OK)
        .send({
          ok: true,
          data: results,
          total: Number(resultTotal[0].total)
        });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  done();

}
