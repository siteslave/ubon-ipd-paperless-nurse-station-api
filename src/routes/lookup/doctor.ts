import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { DoctorService } from '../../models/lookup/doctor';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const doctorService = new DoctorService();

  fastify.get('/', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

    
      const data:any = await doctorService.list(db);
      const rsTotal: any = await doctorService.listTotal(db);

      return reply.status(StatusCodes.CREATED).send({ ok: true ,data,total: Number(rsTotal[0].total)});

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  done();

} 
