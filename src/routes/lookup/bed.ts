import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { BedService } from '../../models/lookup/bed';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const bedService = new BedService();

  //List
  fastify.get('/', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data:any = await bedService.list(db);
      const rsTotal: any = await bedService.listTotal(db);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data,total: Number(rsTotal[0].total)});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //List By Id
  fastify.get('/:bed_id/listByID', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const id:any = req.params.bed_id;
    try {
      const data:any = await bedService.listByID(db,id);

      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //Save
  fastify.post('/', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const datas:object = req.body;
    try {
      const data:any = await bedService.save(db,datas);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })  

  //Update
  fastify.put('/:bed_id', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const datas:object = req.body;
    const id:any = req.params.bed_id;

    try {
      const data:any = await bedService.update(db,datas,id);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })  

  //Delete
  fastify.delete('/:bed_id', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const id:any = req.params.bed_id;

    try {
      const data:any = await bedService.delete(db,id);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  }) 

  done();

} 
