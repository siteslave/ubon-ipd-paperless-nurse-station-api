import { FastifyInstance } from "fastify";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  // verify jwt token
  fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.register(require('./bed'), { prefix: '/lookup/bed' });
  fastify.register(require('./department'), { prefix: '/lookup/department' });
  fastify.register(require('./doctor'), { prefix: '/lookup/doctor' });
  fastify.register(require('./insurance'), { prefix: '/lookup/insurance' });
  fastify.register(require('./ward'), { prefix: '/lookup/ward' });

  done();

} 
