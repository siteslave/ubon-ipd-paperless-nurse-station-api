import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { WardService } from '../../models/lookup/ward';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const wardService = new WardService();

  fastify.get('/', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data:any = await wardService.list(db);
      const rsTotal: any = await wardService.listTotal(db);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data,total: Number(rsTotal[0].total)});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //List By Id
  fastify.get('/:waed_id/listByID', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const id:any = req.params.waed_id;
    try {
      const data:any = await wardService.listByID(db,id);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //List By DepartmentID
  fastify.get('/:department_id/listByDepartment', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const id:any = req.params.department_id;
    try {
      const data:any = await wardService.listByDepartmentId(db,id);

      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //Save
  fastify.post('/', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const datas:object = req.body;
    try {
      const data:any = await wardService.save(db,datas);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })  

  //Update
  fastify.put('/:waed_id', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const datas:object = req.body;
    const id:any = req.params.waed_id;

    try {
      const data:any = await wardService.update(db,datas,id);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })  

  //Delete
  fastify.delete('/:waed_id', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      fastify.guard.scope('order.create','admit.read')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const id:any = req.params.waed_id;

    try {
      const data:any = await wardService.delete(db,id);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  }) 


  done();

} 
