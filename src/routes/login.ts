import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

import { LoginService } from '../models/login';
import loginSchema from '../schema/login';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const loginService = new LoginService();

  fastify.post('/login', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: loginSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const body: any = request.body;
      const { username, password } = body;

      const result: any = await loginService.checkLogin(db, username);

      if (_.isEmpty(result)) {
        return reply.status(StatusCodes.UNAUTHORIZED)
          .send(getReasonPhrase(StatusCodes.UNAUTHORIZED));
      }

      const hash: any = result.password_hash;

      const match: any = await fastify.verifyPassword(password, hash);

      if (!match) {
        return reply.status(StatusCodes.UNAUTHORIZED)
          .send(getReasonPhrase(StatusCodes.UNAUTHORIZED));
      }

      const userId: any = result.id;
      const departmentId: any = result.department_id;
      const wardId: any = result.ward_id;

      var roles: any = []; // ['admin', 'doctor']
      var scopes: any = []; // ['admin.create', 'order.read']

      result.roles.forEach((v: any) => {
        roles.push(v.role);
        v.scope.forEach((x: any) => {
          scopes.push(x);
        });
      });

      const payload: any = {
        sub: userId,
        role: roles,
        scope: scopes,
        department:departmentId,
        ward:wardId
      }

      // Sign new JWT token
      const accessToken = fastify.jwt.sign(payload,{ expiresIn: '1d' });
      return reply.status(StatusCodes.OK).send({ accessToken });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  done();

} 
