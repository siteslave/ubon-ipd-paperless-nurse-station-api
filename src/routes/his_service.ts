import { AxiosResponse } from "axios";
import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify";
import { StatusCodes } from "http-status-codes";
import { HISService } from "../models/his_service";
import * as jwt from 'jsonwebtoken';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const hisService = new HISService();

  // verify jwt token
  fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.get('/his-services/waiting-list', async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const query: any = request.query;
      const { an } = query;

      const userId: any = request.user.sub;

      const hisSecretKey: any = process.env.API_HIS_SECRET_KEY || '';

      var token: any = jwt.sign({
        sub: userId,
        exp: Math.floor(Date.now() / 1000) + (2 * 60), // 2 minute
      }, hisSecretKey);

      const response: AxiosResponse = await hisService.getWaitingList(fastify.axios, token, an);

      const data: any = response.data.data;

      reply.status(StatusCodes.OK).send(data);
    } catch (error: any) {
      request.log.error(error);
      reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
    }
  })

  done();

} 
