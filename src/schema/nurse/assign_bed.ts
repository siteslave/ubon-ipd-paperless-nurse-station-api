import S from 'fluent-json-schema'

const paramsSchema = S.object()
  .prop('id', S.string().format('uuid').required())


const Schema = S.object()

  .prop('bed_id', S.string().format('uuid').required())
  .prop('old_id', S.string().format('uuid').required())

export default {
  params: paramsSchema,
  body: Schema
}