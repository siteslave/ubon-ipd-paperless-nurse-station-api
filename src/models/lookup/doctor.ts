import { Knex } from 'knex';
export class DoctorService {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  list(db: Knex) {
    let sql = db('user')
    return sql.where('is_active',true).orderBy('name')
  }

  listTotal(db: Knex) {
    let sql = db('user')
    return sql.where('is_active',true).count({ total: '*' });
  }
}