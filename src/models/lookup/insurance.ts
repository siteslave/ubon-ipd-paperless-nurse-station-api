import { UUID } from 'crypto';
import { Knex } from 'knex';
export class InsuranceService {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  list(db: Knex) {
    let sql = db('insurance')
    return sql.orderBy('name')
  }

  listTotal(db: Knex) {
    let sql = db('insurance')
    return sql.count({ total: '*' });
  }

  listByID(db: Knex,id:UUID) {
    let sql = db('insurance')
    return sql.where('id',id).orderBy('name')
  }

  save(db: Knex,data:object){
    let sql = db('insurance')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('insurance')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('insurance')
    return sql.delete().where('id',id);
  }  
}