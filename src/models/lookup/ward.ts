import { UUID } from 'crypto';
import { Knex } from 'knex';
export class WardService {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  list(db: Knex) {
    let sql = db('ward')
    return sql.where('is_active',true).orderBy('name')
  }

  listTotal(db: Knex) {
    let sql = db('ward')
    return sql.where('is_active',true).count({ total: '*' });
  }


  listByID(db: Knex,id:UUID) {
    let sql = db('ward')
    return sql.where('is_active',true).where('id',id).orderBy('name')
  }

  listByDepartmentId(db: Knex,department_id:UUID) {
    let sql = db('ward')
    return sql.where('is_active',true).where('department_id',department_id).orderBy('name')
  }

  save(db: Knex,data:object){
    let sql = db('ward')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('ward')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('ward')
    return sql.delete().where('id',id);
  }  
}