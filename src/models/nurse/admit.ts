import { UUID } from 'crypto';
import { Knex } from 'knex';
export class AdmitService {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  save(db: Knex, data: object) {
    return db('admit')
      .insert(data);
  }

  /**
   * 
   * @param db 
   * @param id 
   * @param data 
   * @returns 
   */
  update(db: Knex, id: UUID, data: object) {
    let sql = db('admit')
      .update(data)
      .where('id', id)
      return sql
  }

  /**
   * 
   * @param db 
   * @param id 
   * @returns
   */
  delete(db: Knex, id: UUID) {
    let sql = db('admit')
      .delete()
      .where('id', id)
      return sql
  }

  /**
   * 
   * @param db 
   * @param wardId
   * @param query 
   * @param limit 
   * @param offset 
   * @returns 
   */
  listActive(db: Knex, wardId: UUID, query: string, limit: number, offset: number) {
    let sql = db('admit as a')
      .select('a.*', 'p.cid', 'p.fname', 'p.lname', 'p.gender', 'p.age')
      .innerJoin('patient as p', 'p.an', 'a.an')

    if (wardId) {
      sql.where(builder => {
        builder.whereRaw('a.ward_id = ?', [wardId])
      })
    }

    if (query) {
      let _query = `%${query}%`
      sql.where(builder => {
        builder.orWhere('p.cid', query)
          .orWhere('a.hn', query)
          .orWhereILike('p.fname', _query)
          .orWhereILike('p.lname', _query)
      })
    }

    return sql
      .where('a.is_active', true)
      .limit(limit).offset(offset);

  }

  listActiveTotal(db: Knex, wardId: UUID, query: string) {
    let sql = db('admit as a')
      .innerJoin('patient as p', 'p.an', 'a.an')

    if (wardId) {
      sql.where(builder => {
        builder.whereRaw('a.ward_id = ?', [wardId])
      })
    }

    if (query) {
      let _query = `%${query}%`
      sql.where(builder => {
        builder.orWhere('p.cid', query)
          .orWhere('a.hn', query)
          .orWhereILike('p.fname', _query)
          .orWhereILike('p.lname', _query)
      })
    }

    return sql
      .where('a.is_active', true)
      .count({ total: '*' });

  }

}