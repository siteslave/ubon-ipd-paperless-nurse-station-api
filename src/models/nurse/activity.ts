import { Knex } from 'knex';

export class ActivityModel {
    
    insertNurseNote(db: Knex, data: any) {
        return db('nurse_note')
          .insert(data);
      }

}