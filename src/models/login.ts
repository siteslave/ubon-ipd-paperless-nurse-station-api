import { Knex } from 'knex';
export class LoginService {
  checkLogin(db: Knex, username: any) {
    return db('user')
      .select('user.id', 'user.password_hash', 'user.roles','profile.department_id','profile.ward_id' )
      .innerJoin('profile','profile.user_id','user.id')
      .where('user.username', username)
      .where('user.status', true)
      .first();
  }
}