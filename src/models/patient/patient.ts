import { UUID } from 'crypto';
import { Knex } from 'knex';
export class BedService {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  list(db: Knex) {
    let sql = db('patient')
    return sql.orderBy('an')
  }

  listTotal(db: Knex) {
    let sql = db('patient')
    return sql.count({ total: '*' });
  }

  listByID(db: Knex,id:UUID) {
    let sql = db('patient')
    return sql.where('id',id).orderBy('an')
  }

  save(db: Knex,data:object){
    let sql = db('patient')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('bepatientd')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('patient')
    return sql.delete().where('id',id);
  }

}